using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class TTSUI : MonoBehaviour
{
    public InputField input;
    public Button     button;
    public Image[]    images;
    public Text       text;
    
    private void Awake()
    {
        button.onClick.AddListener (() =>
        {
            var num  = int.Parse (input.text);
            var list = new List<int> ();
            for (var i = 0; i < num; i++)
                list.Add (i);
            text.text = Tools.LogList (Tools.Shuffle (list));
        });
    }

    private void Update()
    {
        foreach (var image in images)
            image.material.SetFloat ("_Y", int.Parse (input.text));
    }
}
