using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
/// <summary>
/// ��̬��AnimationTrack��Animator
/// </summary>
[ExecuteAlways]
[RequireComponent(typeof(PlayableDirector))]
public class AnimatorTrackBinder : MonoBehaviour
{
    private PlayableDirector director;
    private TimelineAsset asset;

    private void OnEnable()
    {
        director = GetComponent<PlayableDirector>();
        asset = director.playableAsset as TimelineAsset;
        for(var i = 0; i < asset.outputTrackCount; i++)
        {
            var track = asset.GetOutputTrack(i);
            if (track.GetType().Name.Equals("AnimationTrack"))
            {
                var animationTrack = track as AnimationTrack;
                var animator = FindTarget(animationTrack);
                if (animator)
                {
                    director.SetGenericBinding(animationTrack, animator);
                }
            }
        }
    }

    private Animator FindTarget(AnimationTrack track)
    {
        var target = GameObject.Find(track.name);
        if (target == null)
            return null;

        if (!target.TryGetComponent<Animator>(out var animator))
            animator = target.AddComponent<Animator>();
        return animator;
    }
}
