﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Text;

public class Tools : MonoBehaviour
{
    private static readonly StringBuilder stringBuilder = new StringBuilder ();
    
#region UI
    public static void ClearChildren (Transform t)
    {
        for (var i = 0; i < t.childCount; i++)
            Destroy (t.GetChild (i).gameObject);
    }

    public static void SetImage (Image image, string path)
    {
        var sprite = Resources.Load<Sprite> (path);
        if (sprite == null)
            Debug.Log ("图片不存在" + path);
        else
            image.sprite = sprite;
    }

    public static void SetImage (GameObject go, string path)
    {
        if (!go.TryGetComponent<Image> (out var image))
            image = go.AddComponent<Image> ();
        SetImage (image, path);
    }

    private static void SetImage3D (Renderer render, string path)
    {
        var texture = Resources.Load<Texture> (path);
        if (texture == null)
            Debug.Log ("图片不存在" + path);
        else
            render.material.mainTexture = texture;
    }

    public static void SetImage3D (GameObject gameObject, string path)
    {
        if (!gameObject.TryGetComponent<MeshRenderer> (out var meshRenderer))
            meshRenderer = gameObject.AddComponent<MeshRenderer> ();
        SetImage3D (meshRenderer, path);
    }

#endregion


#region Calculate
    public static List<int> Shuffle (IList<int> list)
    {
        var newList = new List<int> ();
        var count   = list.Count;
        for (var i = 0; i < count; i++)
        {
            var random = Random.Range (0, list.Count);
            newList.Add (list[random]);
            list.RemoveAt (random);
        }
        return newList;
    }
#endregion
    
    #region Log
        public static string LogList<T> (IList<T> list)
        {
            stringBuilder.Clear ();
            foreach (var item in list)
            {
                stringBuilder.Append (item);
                stringBuilder.Append (",");
            }
            stringBuilder.Replace (",", ".", stringBuilder.Length - 1, 0);
            return stringBuilder.ToString ();
        }
        #endregion
}
