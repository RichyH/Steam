﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bag : MonoBehaviour
{
    public BagUI ui;
    Dictionary<MineType, int> mineral = new Dictionary<MineType, int>();
    public void Add(MineType type,int num)
    {
        if (mineral.ContainsKey(type))
        {
            mineral[type] += num;
            ui.AddItem(type, mineral[type]);
        }
        else
        {
            mineral.Add(type, num);
            ui.NewItem(type, num);
        }
    }
}
