﻿using System.Collections.Generic;
using UnityEngine;

public class Panel_BattleCtrl:MonoBehaviour
{
    List<Card> data = new List<Card>();

    public void Start()
    {
        TFT();
    }

    public void TFT()
    {
        InitData();
        data.Sort(delegate (Card a, Card b) {
            return string.Compare(a.Name, b.Name);
        });
        data.Sort(delegate (Card a, Card b) {
            return a.Price.CompareTo(b.Price);
        });

        Transform t_Card;
        Transform[] t_Price = new Transform[5];
        t_Card = transform.Find("Card");
        for (int i = 0; i < 5; i++)
        {
            t_Price[i] = transform.Find("Price/" + (i + 1));
        }
        foreach (Card card in data)
        {
            Transform t = Instantiate(t_Card);
            t.SetParent(t_Price[card.Price - 1]);
            t.GetComponent<CardUI>().SetUI(card,this);
        }
    }

    void InitData()
    {
        data.Add(new Card(1, "卡牌", Race.腥红, Profession.法师));
        data.Add(new Card(1, "豹女", Race.三国, Profession.神射));
        data.Add(new Card(1, "薇恩", Race.夜幽, Profession.神射));
        data.Add(new Card(1, "塔姆", Race.福星, Profession.斗士));
        data.Add(new Card(1, "大树", Race.森林, Profession.斗士).Debuff());
        data.Add(new Card(1, "盖伦", Race.三国, Profession.重装));
        data.Add(new Card(1, "猴子", Race.天神, Profession.重装).Debuff());
        data.Add(new Card(1, "皎月", Race.月神, Profession.刺客));
        data.Add(new Card(1, "冰女", Race.月神, Profession.耀光));
        data.Add(new Card(1, "蜘蛛", Race.腥红, Profession.神盾));
        data.Add(new Card(1, "亚索", Race.浪人, Profession.决斗));
        data.Add(new Card(1, "剑姬", Race.剑仙, Profession.决斗).Debuff());
        data.Add(new Card(1, "娜美", Race.剑仙, Profession.法师).Debuff());
        data.Add(new Card(2, "蔚", Race.三国, Profession.斗士).Debuff());
        data.Add(new Card(2, "锤石", Race.夜幽, Profession.重装));
        data.Add(new Card(2, "皇子", Race.三国, Profession.神盾).Debuff());
        data.Add(new Card(2, "武器", Race.天神, Profession.决斗).Debuff());
        data.Add(new Card(2, "璐璐", Race.森林, Profession.法师).Debuff());
        data.Add(new Card(2, "塞拉斯", Race.月神, Profession.斗士).Debuff());
        data.Add(new Card(2, "厄斐琉斯", Race.月神, Profession.猎人));
        data.Add(new Card(2, "风女", Race.剑仙, Profession.秘术));
        data.Add(new Card(2, "人马", Race.森林, Profession.重装));
        data.Add(new Card(2, "安妮", Race.福星, Profession.法师));
        data.Add(new Card(2, "提莫", Race.明昼, Profession.神射).Debuff());
        data.Add(new Card(2, "劫", Race.忍者, Profession.夜影).Debuff());
        data.Add(new Card(2, "派克", Race.腥红, Profession.刺客).Debuff());
        data.Add(new Card(3, "寡妇", Race.腥红, Profession.夜影));
        data.Add(new Card(3, "千珏", Race.明昼, Profession.猎人).Debuff());
        data.Add(new Card(3, "光辉", Race.天神, Profession.耀光).Debuff());
        data.Add(new Card(3, "努努", Race.森林, Profession.斗士));
        data.Add(new Card(3, "刀妹", Race.天神, Profession.宗师,Race.剑仙).Debuff());
        data.Add(new Card(3, "猫咪", Race.明昼, Profession.秘术));
        data.Add(new Card(3, "小法", Race.森林, Profession.法师));
        data.Add(new Card(3, "滑板鞋", Race.腥红, Profession.决斗));
        data.Add(new Card(3, "金克丝", Race.福星, Profession.神射).Debuff());
        data.Add(new Card(3, "卡特", Race.福星, Profession.刺客,Race.三国).Debuff());
        data.Add(new Card(3, "阿卡丽", Race.忍者, Profession.刺客));
        data.Add(new Card(3, "凯南", Race.忍者, Profession.神盾).Debuff());
        data.Add(new Card(3, "赵信", Race.三国, Profession.决斗));
        data.Add(new Card(4, "男刀", Race.剑仙, Profession.刺客));
        data.Add(new Card(4, "寒冰", Race.森林, Profession.猎人));
        data.Add(new Card(4, "剑魔", Race.腥红, Profession.重装).Debuff());
        data.Add(new Card(4, "狐狸", Race.明昼, Profession.法师));
        data.Add(new Card(4, "烬", Race.腥红, Profession.神射));
        data.Add(new Card(4, "猪妹", Race.福星, Profession.重装).Debuff());
        data.Add(new Card(4, "瑞文", Race.夜幽, Profession.神盾));
        data.Add(new Card(4, "蛇女", Race.夜幽, Profession.秘术).Debuff());
        data.Add(new Card(4, "莫甘娜", Race.剑仙, Profession.耀光));
        data.Add(new Card(4, "狼人", Race.天神, Profession.猎人, Profession.斗士));
        data.Add(new Card(4, "慎", Race.忍者, Profession.秘术,Profession.宗师).Debuff());
        data.Add(new Card(5, "瑟提", Race.霸王, Profession.斗士).Debuff());
        data.Add(new Card(5, "永恩", Race.浪人, Profession.宗师).Debuff());
        data.Add(new Card(5, "莉莉娅", Race.夜幽, Profession.法师).Debuff());
        data.Add(new Card(5, "沙皇", Race.三国, Profession.神盾, Profession.枭雄).Debuff());
        data.Add(new Card(5, "凯隐", Race.天煞, Profession.夜影));
        data.Add(new Card(5, "盲僧", Race.天神, Profession.决斗).Debuff());
        data.Add(new Card(5, "时光", Race.腥红, Profession.秘术));
        data.Add(new Card(5, "伊泽", Race.森林, Profession.耀光).Debuff());
    }
}









