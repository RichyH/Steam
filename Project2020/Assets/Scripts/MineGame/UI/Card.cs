﻿using UnityEngine;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

public enum Race
{
    霸王, 福星, 浪人, 明昼, 夜幽, 忍者, 三国, 天煞, 天神, 腥红, 森林, 剑仙, 月神
}

public enum Profession
{
    刺客, 斗士, 决斗, 猎人, 秘术, 法师, 神盾, 神射, 枭雄, 耀光, 夜影, 重装, 宗师
}

public class Card
{
    public int Price;
    public string Name;
    public bool debuff;
    public List<Race> Race = new List<Race>();
    public List<Profession> Pro = new List<Profession>();
    public Card(int price, string name, Race race, Profession pro)
    {
        Price = price;
        Name = name;
        Race.Add(race);
        Pro.Add(pro);
    }
    public Card(int price, string name, Race race, Profession pro, Race race2)
    {
        Price = price;
        Name = name;
        Race.Add(race);
        Pro.Add(pro);
        Race.Add(race2);
    }
    public Card(int price, string name, Race race, Profession pro, Profession pro2)
    {
        Price = price;
        Name = name;
        Race.Add(race);
        Pro.Add(pro);
        Pro.Add(pro2);
    }
    public Card Debuff(){
        debuff = true;
        return this;
    }
    public void Log()
    {
        if (Race.Count > 1)
        {
            Debug.Log(Price + "费卡：" + Name + "——" + Race[0] + Race[1] + Pro[0]);
        }
        else
        {
            if (Pro.Count > 1)
            {
                Debug.Log(Price + "费卡：" + Name + "——" + Race[0] + Pro[0] + Pro[1]);
            }
            else
            {
                Debug.Log(Price + "费卡：" + Name + "——" + Race[0] + Pro[0]);
            }
        }
    }
}
