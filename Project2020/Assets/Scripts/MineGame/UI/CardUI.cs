﻿using UnityEngine;
using UnityEngine.UI;
//拖动操作
public class CardUI : MonoBehaviour
{
    Panel_BattleCtrl root;
    int index;
    Vector2 Pos;
    Transform parent,clone;
    public ETCTouchPad touch;
    public Text Name, Race, Race2, Pro, Pro2;

    void Awake()
    { 
        touch.onMove.AddListener(delegate (Vector2 v)
        {
           transform.position = Input.mousePosition;
        });
        touch.onMoveStart.AddListener(delegate ()
        {
            parent = transform.parent;
            Pos = transform.localPosition;
            index = transform.GetSiblingIndex();
            
            clone = Instantiate(gameObject, parent).transform;
            clone.SetSiblingIndex(index);

            transform.SetParent(root.transform);
            transform.localScale *= 2;
        });
        touch.onTouchUp.AddListener(delegate ()
        {
            transform.SetParent(parent);
            transform.localPosition = Pos;
            transform.SetSiblingIndex(index);
            transform.localScale /= 2;
            Destroy(clone.gameObject);
        });
    }

    public void SetUI(Card card,Panel_BattleCtrl root)
    {
        this.root = root;
        name = card.Name;
        Name.text = card.Name;
        Race.text = card.Race[0].ToString();
        Pro.text = card.Pro[0].ToString();
        Race2.gameObject.SetActive(card.Race.Count > 1);
        if (card.Race.Count > 1)
        {
            Race2.text = card.Race[1].ToString();
            
        }
        Pro2.gameObject.SetActive(card.Pro.Count > 1);
        if (card.Pro.Count > 1)
        {
            Pro2.text = card.Pro[1].ToString();
        }
        if (card.debuff)
        {
            GetComponent<Image>().color = Color.yellow;
        }
        gameObject.SetActive(true);
        transform.SetParent(root.transform);
        transform.position = new Vector2(card.Race[0].GetHashCode() * 110+50, card.Pro[0].GetHashCode() * 83+40);
    }
}
