﻿using UnityEngine;
using UnityEngine.UI;

public class BagUI : MonoBehaviour
{
    public GameObject Item;
    public void NewItem(MineType type,int num)
    {
        var item = Instantiate(Item, transform);
        item.name = type.ToString();
        Tools.SetImage(item, "Textures/Mine/" + (int)type);
        item.GetComponentInChildren<Text>().text = num.ToString();
        item.SetActive(true);
    }

    public void AddItem(MineType type, int num)
    {
        var item = transform.Find(type.ToString());
        item.GetComponentInChildren<Text>().text = num.ToString();
    }
}
