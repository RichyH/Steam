﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MinePop : MonoBehaviour
{
    public Text Name, Count;
    public Image Icon;
    public Button Mine;
    public Bag bag;

    public void Open(Mine mine)
    {
        Name.text = mine.Type.ToString();
        Count.text = "储量" + mine.Num;
        Tools.SetImage(Icon, "Textures/Mine/" + (int)mine.Type);
        gameObject.SetActive(true);
        Mine.interactable = mine.Num > 0;
        Mine.onClick.RemoveAllListeners();
        Mine.onClick.AddListener(() => {
            Debug.Log("获得" + mine.Type);
            bag.Add(mine.Type, 1);
            mine.Num -= 1;
            mine.SetNum();
            Open(mine);
        });
    }
}
