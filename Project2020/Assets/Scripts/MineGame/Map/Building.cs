﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct _Building
{
    int ID;
    string Name;
    Dictionary<int, int> Material;
    int[] Output;
    int TechLevel;
}

public class Building : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
