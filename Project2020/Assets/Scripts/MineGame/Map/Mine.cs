﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MineType
{
    木,铁,钻,铜,铝,石
}

public class Mine : MonoBehaviour
{
    public int Num;
    public MineType Type;
    Material material;

    private void Awake()
    {
        material = GetComponent<MeshRenderer>().materials[0];
    }

    public void Init(int type,int num)
    {
        Type = (MineType)type;
        Tools.SetImage3D(gameObject, "Textures/Mine/" + type);
        SetNum(num);
    }

    public void SetNum(int i)
    {
        if (i <= 0)
        {
            gameObject.SetActive(false);
        }
        Num = i;
        material.mainTextureScale = new Vector2(0.125f, 0.125f);
        material.mainTextureOffset = new Vector2(0.125f, 0.125f * (Num / 16));
    }

    public void SetNum()
    {
        SetNum(Num);
    }
}
