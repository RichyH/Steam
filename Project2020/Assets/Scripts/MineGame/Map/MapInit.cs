﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class MapInit : MonoBehaviour
{
    public SquareGrid grid;
    // Start is called before the first frame update
    void Start()
    {
        System.Random random = new System.Random(1);
        for (int i = -20; i < 20; i++)
        {
            for (int j = -20; j < 20; j++)
            {
                if (random.Next(0, 4) < 1)
                {
                    SquareGrid H = Instantiate(grid.gameObject).GetComponent<SquareGrid>();
                    H.name = "Obstacle";
                    H.x = i;
                    H.y = j;
                    //矿物
                    if(random.Next(0, 4) < 1)
                    {
                        H.gameObject.AddComponent<Mine>().Init(random.Next(0, 6), random.Next(0, 128));
                    }
                }
            }
        }
    }
}
