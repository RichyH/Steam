﻿using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class SquareGrid : MonoBehaviour
{
    public int x, y;
    static float g3 = Mathf.Sqrt(3);

    public static Vector2 Grid2Real(Vector2 v)
    {
        return Grid2Real(v.x, v.y);
    }
    public static Vector2 Grid2Real(float x, float y)
    {
        return new Vector2(x,y);
    }

    public static Vector2 Real2Grid(float x,float y)
    {
        return new Vector2(Mathf.Floor(x + 0.5f), Mathf.Floor(y + 0.5f));
    }
    private void Update()
    {
        //Vector2 v = Grid2Real(x, y);
        transform.position = new Vector3(x, 0.01f, y);
    }
}
