﻿using UnityEngine;

public enum NodeState
{
    UnClear,//未知
    Clear,//无雷
    Warning,//有雷警戒
    Boom,//爆炸
}

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class HexagonNode : MonoBehaviour
{
    public Vector2 p;
    public Material[] materials;
    public bool hasBoom;
    private MeshRenderer render;
    private static readonly float g3 = Mathf.Sqrt(3);
    private Vector2 x = new Vector2(1.5f, -g3 / 2);
    private Vector2 y = Vector2.up * g3;
    private TextMesh text;
    public NodeState state;

    private void OnEnable()
    {
        var mesh = GetComponent<MeshFilter>().mesh;
        mesh.name = "正六边形";
        mesh.vertices = new Vector3[] {
            new Vector3(1,  0),
            new Vector3(0.5f,  -g3/2),
            new Vector3(-0.5f,  -g3/2),
            new Vector3(-1, 0),
            new Vector3(-0.5f,  g3/2),
            new Vector3(0.5f,  g3/2),
        };
        mesh.triangles = new int[] { 0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5 };
        mesh.uv = new Vector2[]
        {
            new Vector2 (1, 0.5f),
            new Vector2 (0.75f, 0.5f - g3 / 4),
            new Vector2 (0.25f, 0.5f - g3 / 4),
            new Vector2 (0, 0.5f),
            new Vector2 (0.25f, 0.5f + g3 / 4),
            new Vector2 (0.75f, 0.5f + g3 / 4)
        };
        text = transform.Find("Text").GetComponent<TextMesh>();
    }

    public void Init(bool isBoom, Vector2 pos)
    {
        p = pos;
        hasBoom = isBoom;
        gameObject.SetActive(true);
        gameObject.name = pos.x + "," + pos.y;
        render = gameObject.GetComponent<MeshRenderer>();
        transform.position = p.x * x + p.y * y;
        name = p.ToString();
        SetState(NodeState.UnClear);
    }

    public void SetState(NodeState state)
    {
        this.state = state;
        switch (state)
        {
            case NodeState.UnClear:
                render.material = materials[0];
                break;
            case NodeState.Clear:
                render.material = materials[1];
                text.text = InitMap.OpenNoBoom(p);
                InitMap.instance.CheckAllClear();
                break;
            case NodeState.Warning:
                render.material = materials[2];
                InitMap.instance.CheckAllClear();
                break;
            case NodeState.Boom:
                render.material = materials[3];
                InitMap.instance.ReStart();
                break;
        }
    }

    public void Open()
    {
        if (state == NodeState.UnClear)
            SetState(hasBoom ? NodeState.Boom : NodeState.Clear);
    }

    public void Warn()
    {
        SetState(2 - state);
    }
}
