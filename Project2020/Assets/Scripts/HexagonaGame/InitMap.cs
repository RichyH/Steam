using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InitMap : MonoBehaviour
{
    public static InitMap instance;
    public HexagonNode item;
    public Material material1;
    public Material material2;
    public InputField r, m;
    public Text time,record;
    int radius, mount;
    public bool isTimer;
    float fast, timer;

    static Dictionary<Vector2, HexagonNode> nodes = new Dictionary<Vector2, HexagonNode>();
    static Vector2[] around = new[] {
        Vector2.up,Vector2.up+Vector2.right, Vector2.right ,
        Vector2.down,Vector2.left, Vector2.down + Vector2.left,};
    readonly Vector2[] dir = new[] { Vector2.right, Vector2.up, Vector2.left + Vector2.down };
    readonly Vector2[] dir2 = new[] { Vector2.up, Vector2.left + Vector2.down, Vector2.right };

    private void Awake()
    {
        instance = this;
        radius = int.Parse(r.text);
        mount = int.Parse(m.text);
    }

    private void OnDestroy()
    {
        instance = null;
    }

    private void Update()
    {
        if (isTimer)
            timer += Time.deltaTime;
        time.text = timer.ToString();
    }

    private void Start()
    {
        r.onValueChanged.AddListener((string text) => {
            radius = int.Parse(text);
        });
        m.onValueChanged.AddListener((string text) => {
            mount = int.Parse(text);
        });
    }

    private void CreateItem(Vector2 v)
    {
        var isBoom = Random.Range(0, 100) < mount;
        var node = Instantiate(item, transform);
        node.Init(isBoom, v);
        nodes.Add(v, node);
    }

    private void OnDisable()
    {
        for (int i = transform.childCount - 1; i >= 0; i--)
            DestroyImmediate(transform.GetChild(i).gameObject);
        nodes.Clear();
    }

    private void OnEnable()
    {
        isTimer = false;
        Camera.main.orthographicSize = radius * 2;
        CreateItem(Vector2.zero);
        for (var i = 1; i <= radius; i++)
            for (int j = 0; j < dir.Length; j++)
                for (int k = 0; k <= radius; k++)
                    CreateItem(dir2[j] * k + dir[j] * i);
    }

    public static string OpenNoBoom(Vector2 p)
    {
        var boom = GetBoomAround(p);
        if (boom == 0)
        {
            for (int i = 0; i < around.Length; i++)
            {
                nodes.TryGetValue(p + around[i], out var node);
                if (node != null)
                    node.Open();
            }
            return "";
        }
        return boom.ToString();
    }

    public static int GetBoomAround(Vector2 p)
    {
        int count = 0;
        for (int i = 0; i < around.Length; i++)
        {
            nodes.TryGetValue(p + around[i], out var node);
            if (node != null && node.hasBoom)
                count++;
        }
        return count;
    }

    public void CheckAllClear()
    {
        isTimer = true;
        foreach (var kv in nodes)
            if (kv.Value.state == NodeState.UnClear)
                return;
        if (fast == 0 || timer < fast)
        {
            record.text = "最快记录：" + timer;
            fast = timer;
        }
        ReStart();
    }

    public void ReStart()
    {
        timer = 0;
        time.text = "";
        gameObject.SetActive(false);
        gameObject.SetActive(true);
    }

    public void Reset()
    {
        fast = 0;
        time.text = "";
        record.text = "最快记录：";
    }

    public static void OpenAround (Vector2 p)
    {
        for (var i = 0; i < around.Length; i++)
        {
            nodes.TryGetValue (p + around[i], out var node);
            if (node != null)
                node.Open ();
        }
    }
}
