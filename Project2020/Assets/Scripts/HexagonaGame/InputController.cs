﻿using UnityEngine;

public class InputController : MonoBehaviour
{
    private float  downTime0, downTime1;
    private float  upTime0,   upTime1;
    private bool   left,      right;
    private Camera cameraMain;

    public float sensibility = 0.5f;

    private void Start()
    {
        cameraMain         = Camera.main;
        transform.position = new Vector3(0, 0, -(float)Screen.height / 100);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            downTime0 = Time.time;
            left = true;
        }
        if (Input.GetMouseButtonDown(1))
        {
            downTime1 = Time.time;
            right = true;
        }
        if (downTime0 * downTime1 != 0 && Mathf.Abs(downTime0 - downTime1) < sensibility)
        {
            downTime0 = 0;
            downTime1 = 0;
        }

        if (Input.GetMouseButtonUp(0))
        {
            upTime0 = Time.time;
            left = false;
            if (!right)
                GetHexagonNode()?.Open();
        }
        if (Input.GetMouseButtonUp(1))
        {
            upTime1 = Time.time;
            right = false;
            if(!left)
                GetHexagonNode()?.Warn();
        }
        if (upTime0 * upTime1 != 0 && Mathf.Abs(upTime0 - upTime1) < sensibility)
        {
            upTime0 = 0;
            upTime1 = 0;
            var node = GetHexagonNode ();
            if (node.state == NodeState.Clear)
                InitMap.OpenAround (node.p);
        }
    }

    private HexagonNode GetHexagonNode()
    {
        var ray = cameraMain.ScreenPointToRay(Input.mousePosition);
        var hit = Physics2D.Raycast(ray.origin, ray.direction);
        return hit.collider?.GetComponent<HexagonNode>();
    }


}
