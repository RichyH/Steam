using System.Text;
using UnityEngine;
using System.Collections.Generic;
//管理所有数据
namespace Agricola
{
    public enum AnimalType
    {
        Sheep, Pig, Ox
    }

    public class Data : MonoBehaviour
    {
        public static Data   instance;
        public        int    playerCount;
        public        int    playerNowId;
        public        Player playerNow => players[playerNowId];

        private          int           startId;
        private          List<int>     playersIndex = new List<int> ();
        public readonly  List<Player>  players      = new List<Player> ();
        private readonly StringBuilder sb           = new StringBuilder ();
        public readonly  List<int>     seasons      = new List<int> ();

        public int wood3;
        public int brick1;
        public int reed1;
        public int fish1;
        public int sheep;
        public int stoneA;
        public int stoneB;

        private void Awake ()
        {
            instance = this;
            InitPlayers ();
            InitSeasons ();
        }

        private void OnDestroy ()
        {
            instance = null;
        }

        private void InitPlayers ()
        {
            players.Clear ();
            playersIndex.Clear ();
            for (var i = 0; i < playerCount; i++)
            {
                players.Add (new Player (i));
                playersIndex.Add (i);
            }
            playersIndex = Tools.Shuffle (playersIndex);
        }

        private void InitSeasons ()
        {
            seasons.Clear ();
            seasons.Add (1);
            seasons.AddRange (Tools.Shuffle (new List<int> {2, 3, 4}));
            seasons.AddRange (Tools.Shuffle (new List<int> {5, 6, 7}));
            seasons.AddRange (Tools.Shuffle (new List<int> {8, 9}));
            seasons.AddRange (Tools.Shuffle (new List<int> {10, 11}));
            seasons.AddRange (Tools.Shuffle (new List<int> {12, 13}));
            seasons.Add (14);
        }
        
        public void SetFirst ()
        {
            startId = playerNowId;
        }

        private void LogListInt (IEnumerable<int> list)
        {
            sb.Clear ();
            foreach (var t in list)
            {
                sb.Append (t);
                sb.Append (',');
            }
            Debug.Log (sb);
        }

        public bool CanBuildHouse ()
        {
            return playerNow.reed >= 2 && (playerNow.wood >= 5 || playerNow.brick >= 5 || playerNow.stone >= 5);
        }
        
        public bool CanBuildStable ()
        {
            return playerNow.wood >= 2;
        }

        public void NextSeason ()
        {
            wood3  += 3;
            brick1 += 1;
            reed1  += 1;
            fish1  += 1;
            stoneA += 1;
            stoneB += 1;
        }
        
        public void GetWood3 ()
        {
            playerNow.wood += wood3;
        }

        public void GetBrick1 ()
        {
            playerNow.brick += brick1;
        }
        
        public void GetReed1 ()
        {
            playerNow.reed += reed1;
        }
        
        public void GetFish1 ()
        {
            playerNow.food += fish1;
        }
        
        public void GetWheat ()
        {
            playerNow.wheat += 1;
        }
        
        public void GetStoneA ()
        {
            playerNow.stone += stoneA;
        }
        
        public void GetStoneB ()
        {
            playerNow.stone += stoneB;
        }
        
        public void GetVegetable ()
        {
            playerNow.vegetables += 1;
        }
        
        public void OddHand ()
        {
            playerNow.food += 2;
        }
        
        public void Plow (int position)
        {
            if (playerNow.farm.areas[position].state == AreaState.None)
                playerNow.farm.areas[position].state = AreaState.Farm;
        }

        public void NewCareer (int id)
        {
            playerNow.Career.Add (id);
        }

        public void NextPlayerSelect ()
        {
            playerNowId += 1;
            if (playerNowId >= players.Count)
                playerNowId = 0;
            foreach (var person in playerNow.family)
            {
                if (person.work == 0)
                {
                    UIMain.instance.SelectToWork(playerNowId, person.index);
                    return;
                }
            }
        }

        public void RenovateHouses (AreaState state)
        {
            foreach (var area in playerNow.farm.areas)
            {
                if (area.IsHouse)
                    area.state = state;
            }
        }
    }
}
