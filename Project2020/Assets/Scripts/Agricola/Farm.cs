namespace Agricola
{
    public enum AreaState
    {
        None,       //空地
        Farm,       //耕地
        HouseWood,  //木屋
        HouseBrick, //砖屋
        HouseStone, //石屋
        Stable,//马厩
    }

    public class Farm
    {
        public class Area
        {
            public AreaState state;
            public Area () => state = AreaState.None;
            public bool IsHouse => (int)state > 1 && (int)state < 5;
        }
        public readonly Area[] areas = new Area[15];

        public Farm ()
        {
            for (var i = 0; i < areas.Length; i++)
                areas[i] = new Area ();
            areas[0].state = AreaState.HouseWood;
            areas[1].state = AreaState.HouseWood;
        }
    }
}
