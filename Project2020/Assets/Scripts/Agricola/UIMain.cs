using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Agricola
{
    public class UIMain : UIExtension
    {
        public static UIMain instance;
        
        private          DraggableItem             person;
        private          Image                     season;
        private          Transform                 houseTypeT;
        private          AreaState                 houseType;
        private          Button                    houseTypeButton;
        private          Button                    nextSeason;
        
        private readonly Transform[]               selectArea      = new Transform[2];
        private readonly Text[][]       resources       = new Text[2][];
        private readonly List<Image[]>             areas           = new List<Image[]> ();
        private readonly List<Image>               seasons         = new List<Image> ();
        private readonly List<List<DraggableItem>> family          = new List<List<DraggableItem>> ();
        private readonly Toggle[]                  houseTypeToggle = new Toggle[3];

        private void Awake ()
        {
            instance      = this;
            season        = Find<Image> ("season");
            person        = Find<DraggableItem> ("person");
            selectArea[0] = Find ("player1/Select");
            selectArea[1] = Find ("player2/Select");
            houseTypeT    = Find ("HouseType");
            for (var i = 0; i < 3; i++)
            {
                var index = i;
                houseTypeToggle[i] = SetToggle ("HouseType/" + i, (isOn) =>
                {
                    if (isOn)
                        houseType = (AreaState)(index + 2);
                });
            }
            houseTypeButton = SetBtn ("HouseType/Button", () =>
            {
                houseTypeT.gameObject.SetActive (false);
                SelectAreaToBuild (true);
            });
            nextSeason   = SetBtn ("NextSeason", NextSeason);
            resources[0] = Find ("player1/Num").GetComponentsInChildren<Text> ();
            resources[1] = Find ("player2/Num").GetComponentsInChildren<Text> ();
            areas.Add(Find ("player1/Farm").GetComponentsInChildren<Image> ());
            areas.Add(Find ("player2/Farm").GetComponentsInChildren<Image> ());
        }

        private void OnDestroy ()
        {
            instance = null;
        }

        private void Work (GameObject man, GameObject work)
        {
            var draggableItem = man.GetComponent<DraggableItem> ();
            int id;
            try
            {
                id = int.Parse (work.name);
            }
            catch (Exception)
            {
                draggableItem.Reset ();
                return;
            }
            if (!Action.Do(id))
                draggableItem.Reset();
            else
            {
                man.transform.SetParent(work.transform);
                man.GetComponent<DraggableItem>().enabled = false;
                var image = work.GetComponent<Image>();
                image.color = Color.grey;
                image.raycastTarget = false;
                Data.instance.playerNow.family[int.Parse(man.name)].work = id;
                Data.instance.NextPlayerSelect();
                //本季结束
                if (family.SelectMany(people => people).Any(p => p.transform.parent.name.Length > 2))
                    return;
                nextSeason.gameObject.SetActive(true);
            }
        }

        private void Start ()
        {
            InitSeasons ();
            Data.instance.NextSeason ();
            ResetFarm (0);
            ResetFarm (1);
        }

        private void InitSeasons ()
        {
            for (var i = 1; i <= 24; i++)
            {
                if (i <= 10)
                {
                    seasons.Add (Find<Image> ("Seasons/0/" + i));
                    Find<Text> ("Seasons/0/" + i + "/Text").text = ((ActionText)(i-1)).ToString ();
                }
                else
                {
                    var s      = Instantiate (season, Find ("Seasons/" + (i - 10)));
                    var action = Data.instance.seasons[i - 11] + 10;
                    s.transform.Find ("Text").GetComponent<Text> ().text = ((ActionText)(action-1)).ToString ();
                    s.name                                                          = action.ToString ();
                    s.gameObject.SetActive (true);
                    seasons.Add (s);
                }
            }
        }

        private void ResetFarm (int playerId)
        {
            UpdateResources (playerId);
            var player = Data.instance.players[playerId];
            var farm   = areas[playerId];
            
            family.Add (new List<DraggableItem> ());
            for (var i = 0; i < player.farm.areas.Length; i++)
            {
                if (player.farm.areas[i].IsHouse && family[player.id].Count < player.family.Count)
                {
                    var p = Instantiate (person, farm[i].transform);
                    SetDragItem (p, null, Work, false);
                    p.gameObject.SetActive (true);
                    p.name = family[player.id].Count.ToString();
                    family[player.id].Add (p);
                }
                UpdateArea (playerId, i);
            }
        }

        private void UpdateArea (int playerId,int position)
        {
            var state = Data.instance.players[playerId].farm.areas[position].state;
            var area     = areas[playerId][position];
            area.GetComponent<Image> ().sprite = GetAreaIcon (state);
            area.GetComponent<Image>().color = Color.white;
        }

        private static Sprite GetAreaIcon (AreaState state) => state switch
        {
            AreaState.HouseWood  => Resources.Load<Sprite> ("Textures/Agricola/House0"),
            AreaState.HouseBrick => Resources.Load<Sprite> ("Textures/Agricola/House1"),
            AreaState.HouseStone => Resources.Load<Sprite> ("Textures/Agricola/House2"),
            AreaState.None       => null,
            AreaState.Farm       => Resources.Load<Sprite> ("Textures/Agricola/Farm"),
            AreaState.Stable     => Resources.Load<Sprite> ("Textures/Agricola/Stable"),
            _                    => null
        };

        public void SelectAreaToBuild (bool houseOrStable)
        {
            var go = selectArea[Data.instance.playerNowId];
            go.gameObject.SetActive (true);
            for (var i = 0; i < go.childCount; i++)
            {
                var pos    = i;
                var toggle = go.GetChild (i).GetComponent<Toggle> ();
                toggle.onValueChanged.RemoveAllListeners ();
                toggle.onValueChanged.AddListener ((isOn) =>
                {
                    if (isOn)
                        Action.NewHouse (pos, houseOrStable ? houseType : AreaState.Stable);
                });
            }
        }

        public void SelectAreaToPlow ()
        {
            
        }
        
        public void SelectAreaToPlowAndSow ()
        {
            
        }

        public void SelectAreaToRear (AnimalType animal)
        {
            
        }
        
        public void SelectAreaToSow ()
        {
            
        }

        public void BuildFences ()
        {
            
        }

        public void SelectHouseType ()
        {
            houseTypeT.gameObject.SetActive (true);
        }

        private void UpdateResources (int playerId)
        {
            var player = Data.instance.players[playerId];
            var texts  = resources[playerId];
            texts[0].text = player.food.ToString ();
            texts[1].text = player.wood.ToString ();
            texts[2].text = player.brick.ToString ();
            texts[3].text = player.stone.ToString ();
            texts[4].text = player.reed.ToString ();
            texts[5].text = player.wheat.ToString ();
            texts[6].text = player.vegetables.ToString ();
            texts[7].text = player.sheep.ToString ();
            texts[8].text = player.pig.ToString ();
            texts[9].text = player.ox.ToString ();
        }

        public void UpdateResources ()
        {
            UpdateResources (Data.instance.playerNowId);
        }

        private void NextSeason ()
        {
            Data.instance.NextSeason ();
            foreach (var p in family.SelectMany (people => people))
                Destroy (p.gameObject);
            family.Clear ();
            ResetFarm (0);
            ResetFarm (1);
            foreach (var image in seasons)
            {
                image.color         = Color.white;
                image.raycastTarget = true;
            }
            nextSeason.gameObject.SetActive(false);
        }
        
        public void NewCareer ()
        {
            
        }

        public void SelectToWork (int playerId, int index)
        {
            for (var i = 0; i < family[playerId].Count; i++)
            {
                family[playerId][i].enabled                                = index == i;
                family[playerId][i].transform.parent.GetComponent<Image> ().color = index == i ? Color.blue : Color.grey;
            }
        }

        public void PlayCard ()
        {
            
        }
    }
}
