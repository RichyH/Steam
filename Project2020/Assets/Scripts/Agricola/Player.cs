using System.Collections.Generic;

namespace Agricola
{
    public class Person
    {
        public int  index;
        public int  work;
        public bool isBaby;
        public Person (int i, bool baby = false)
        {
            index  = i;
            work   = 0;
            isBaby = baby;
        }
}
    
    public class Player
    {
        public readonly int id;
        
        public int          food;
        public int          wood;
        public int          brick;
        public int          stone;
        public int          reed;
        public int          wheat;
        public int          vegetables;
        public int          sheep;
        public int          pig;
        public int          ox;
        public List<int>    Career;
        public List<Person> family;

        public readonly Farm farm;

        public Player (int i)
        {
            id     = i;
            food   = i == 0 ? 2 : 3;
            farm   = new Farm ();
            Career = new List<int> ();
            family = new List<Person> {new Person (0), new Person (1)};
        }
        
        public void HaveChild (bool needHouse = true)
        {
            family.Add (new Person (family.Count));
        }
    }
}
