using UnityEngine;

namespace Agricola
{

    public enum ActionText
    {
        扩建房屋修建马厩,
        抢先打一张次要发展卡,
        拿一份麦子,
        犁一块农田,
        打一张职业卡,
        临时工,
        木头,
        砖头,
        芦苇,
        钓鱼,
        
        打一张发展卡,
        羊,
        播种烤面包,
        建造围栏,
        
        石头A,
        生孩子次要发展卡,
        翻修房屋发展卡,
        
        蔬菜,
        野猪,
        
        牛,
        石头B,
        
        犁一块地播种,
        无房生孩子,
        
        翻修房屋建造围栏
    }

    public static class 
        Action
    {
        public static bool NewHouse (int position, AreaState houseType)
        {
            var player = Data.instance.playerNow;
            if (player.farm.areas[position].state != AreaState.None)
                return false;

            if (player.reed < 2)
                return false;

            switch (houseType)
            {
                case AreaState.HouseWood:
                    if (player.wood < 5)
                        return false;

                    player.wood                       -= 5;
                    player.reed                       -= 2;
                    player.farm.areas[position].state =  AreaState.HouseWood;
                    return true;
                case AreaState.HouseBrick:
                    if (player.brick < 5)
                        return false;

                    player.brick                      -= 5;
                    player.reed                       -= 2;
                    player.farm.areas[position].state =  AreaState.HouseBrick;
                    return true;
                case AreaState.HouseStone:
                    if (player.stone < 5)
                        return false;

                    player.stone                      -= 5;
                    player.reed                       -= 2;
                    player.farm.areas[position].state =  AreaState.HouseStone;
                    return true;
                case AreaState.Stable:
                    if (player.wood < 2)
                        return false;

                    player.wood                       -= 2;
                    player.farm.areas[position].state =  AreaState.Stable;
                    return true;
                default: return false;
            }
        }

        public static bool Do (int actionId)
        {
            bool canDo;
            switch (actionId)
            {
                case 1:
                    if (Data.instance.CanBuildHouse())
                        UIMain.instance.SelectHouseType();
                    else if (Data.instance.CanBuildStable())
                        UIMain.instance.SelectAreaToBuild(false);
                    break;
                case 2: 
                    Data.instance.SetFirst ();
                    break;
                case 3:
                    Data.instance.GetWheat ();
                    break;
                case 4:
                    UIMain.instance.SelectAreaToPlow ();
                    break;
                case 5:
                    UIMain.instance.NewCareer ();
                    break;
                case 6:
                    Data.instance.OddHand ();
                    break;
                case 7:
                    Data.instance.GetWood3 ();
                    UIMain.instance.UpdateResources ();
                    break;
                case 8:
                    Data.instance.GetBrick1 ();
                    UIMain.instance.UpdateResources ();
                    break;
                case 9:
                    Data.instance.GetReed1 ();
                    UIMain.instance.UpdateResources ();
                    break;
                case 10:
                    Data.instance.GetFish1 ();
                    UIMain.instance.UpdateResources ();
                    break;
                case 11:
                    UIMain.instance.PlayCard ();
                    break;
                case 12:
                    UIMain.instance.SelectAreaToRear (AnimalType.Sheep);
                    break;
                case 13:
                    UIMain.instance.SelectAreaToSow ();
                    break;
                case 14: 
                    UIMain.instance.BuildFences ();
                    break;
                case 15:
                    Data.instance.GetStoneA ();
                    break;
                case 16:
                    Data.instance.playerNow.HaveChild ();
                    break;
                case 17:
                    UIMain.instance.SelectHouseType ();
                    break;
                case 18:
                    Data.instance.GetVegetable ();
                    break;
                case 19: 
                    UIMain.instance.SelectAreaToRear (AnimalType.Pig);
                    break;
                case 20: 
                    UIMain.instance.SelectAreaToRear (AnimalType.Ox);
                    break;
                case 21: 
                    Data.instance.GetStoneB ();
                    break;
                case 22:
                    UIMain.instance.SelectAreaToPlowAndSow ();
                    break;
                case 23: 
                    Data.instance.playerNow.HaveChild (false);
                    break;
                case 24: 
                    UIMain.instance.SelectHouseType ();
                    UIMain.instance.BuildFences ();
                    break;
            }
            canDo = true;
            return canDo;
        }
    }
}