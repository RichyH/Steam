﻿using System;
using UnityEngine;

public class TetrisArea : MonoBehaviour
{
    public float      speed1 = 2;
    public float      speed2 = 10;
    public TetrisItem origin;
    
    private TetrisItem  current;

    private void Awake ()
    {
        Physics2D.gravity = Vector3.right;
    }

    private void Start ()
    {
        NewItem ();
        GetComponent<ETCButton> ().onDown.AddListener (delegate { current.SpeedUp (Input.mousePosition.y - (float)Screen.height / 2); });
    }
    
    public void NewItem ()
    {
        current = Instantiate (origin,transform);
        current.Init (this);
    }
}
