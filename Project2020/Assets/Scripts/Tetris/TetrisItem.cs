﻿using UnityEngine;

public class TetrisItem : MonoBehaviour
{
    public  float      speed;
    private TetrisArea area;
    private float      radius;

    public void Init (TetrisArea area)
    {
        this.area            =  area;
        speed                =  area.speed1;
        radius               =  Random.Range (1, 6);
        transform.localScale *= radius * 50;
        gameObject.SetActive (true);
    }

    private void Update ()
    {
        transform.localPosition += Vector3.right * (Time.deltaTime * speed);
    }

    private void OnCollisionEnter2D (Collision2D other)
    {
        if (other.gameObject.name == radius.ToString ())
        {
            other.transform.localScale *= 2;
            area.NewItem ();
            Destroy (gameObject);
        }
        else if (other.gameObject.name != "Wall")
        {
            speed = 0;
            name  = radius.ToString ();
            Destroy (this);
            if (transform.localPosition.x > -700)
                area.NewItem ();
            else
            {
                Destroy (gameObject);
                area.NewItem ();
            }
        }
    }

    public void SpeedUp (float y)
    {
        y = Mathf.Floor (y / 100) * 100 + 50;
        speed                   = area.speed2;
        transform.localPosition = new Vector2 (transform.localPosition.x,y);
    }
}
