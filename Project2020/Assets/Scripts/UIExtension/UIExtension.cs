﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class UIExtension : MonoBehaviour
{
    public void Log (object o)
    {
        Debug.Log (o);
    }


#region Find
    protected Transform Find (string path)
    {
        return transform.Find (path);
    }

    protected T Find<T> (string path)
    {
        return Find (path).GetComponent<T> ();
    }

    protected GameObject FindGameObject (string path)
    {
        return Find (path).gameObject;
    }

    /// 查找某一个节点下所有孩子节点上的某个组件，返回集合 
    protected T[] Finds<T> (string path)
    {
        return Find (path).GetComponentsInChildren<T> ();
    }
#endregion


#region Set
    protected ETCButton SetETCBtn (string path, UnityAction action)
    {
        var btn = Find<ETCButton> (path);
        btn.onUp.AddListener (action);
        return btn;
    }

    protected Button SetBtn (string path, Action action)
    {
        var btn = Find<Button> (path);
        btn.onClick.AddListener (() =>
        {
            if (btn.GetComponent<DraggableItem> () == null || btn.GetComponent<DraggableItem> ().dropTarget == null)
                action ();
        });
        return btn;
    }

    protected DraggableItem SetDragItem (string path, string dropName, Action<GameObject, GameObject> callback, bool reset = true)
    {
        var t = Find (path);
        return t == null ? null : SetDragItem (t, dropName, callback, reset);
    }

    protected DraggableItem SetDragItem (Transform t, string dropName, Action<GameObject, GameObject> callback, bool reset = true)
    {
        if (!t.TryGetComponent<DraggableItem> (out var item))
            item = t.gameObject.AddComponent<DraggableItem> ();
        return SetDragItem(item,dropName,callback,reset);
    }

    protected static DraggableItem SetDragItem (DraggableItem item, string dropName, Action<GameObject, GameObject> callback, bool reset = true)
    {
        item.dropName = dropName;
        item.callback = callback;
        item.reset    = reset;
        return item;
    }

    protected Toggle SetToggle (string path, UnityAction<bool> callback)
    {
        var toggle = Find<Toggle> (path);
        toggle.onValueChanged.AddListener (callback);
        return toggle;
    }

    protected SmartScrollView SetScrollView (string path, int dataCount, Action<Transform, int> callback)
    {
        var ssv = Find<SmartScrollView> (path);
        ssv.dataCount = dataCount;
        ssv.callback  = callback;
        return ssv;
    }
#endregion
}
