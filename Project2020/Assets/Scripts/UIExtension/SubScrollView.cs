using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// 双层嵌套样式ScrollView，横竖都可以划动
/// </summary>
public class SubScrollView : ScrollRect
{
    public ScrollRect rootScrollRect;
    public float xDelta;
    public bool useRoot;

    protected override void Awake()
    {
        rootScrollRect = transform.parent.gameObject.GetComponentInParent<ScrollRect>();
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        useRoot = Mathf.Abs(eventData.delta.x) > xDelta;
        if (useRoot)
            rootScrollRect.OnBeginDrag(eventData);
        else
            base.OnBeginDrag(eventData);
    }

    public override void OnDrag(PointerEventData eventData)
    {
        if (useRoot)
            rootScrollRect.OnDrag(eventData);
        else
            base.OnDrag(eventData);
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        if (useRoot)
            rootScrollRect.OnEndDrag(eventData);
        else
            base.OnEndDrag(eventData);
        useRoot = false;
    }
}
