using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public enum ScrollType
{
    Horizontal,
    Vertical,
    HorizontalGrid,
    VerticalGrid
}

public class SmartScrollView : ScrollRect
{
#region Fileds
    public bool                   loop;
    public ScrollType             scrollType;
    public int                    dataCount;
    public Action<Transform, int> callback;

    private Vector2       viewportSize;
    private Vector2       contentPosition;
    private RectTransform item;
    private float         spacing;
    private int           dragCount;
    private int           start, end;
    private int           constraintCount;
#endregion


#region Override
    protected override void Awake ()
    {
        inertia      = false;
        movementType = MovementType.Unrestricted;
        if (viewport == null)
            viewport = GetComponentInChildren<Mask> ().GetComponent<RectTransform> ();

        if (content == null)
            content = GetComponentInChildren<ContentSizeFitter> ().GetComponent<RectTransform> ();

        item                  = (RectTransform)transform.Find ("Item");
        item.pivot            = Vector2.up;
        item.anchorMin        = Vector2.up;
        item.anchorMax        = Vector2.up;
        item.anchoredPosition = Vector2.zero;
    }

    protected override void OnEnable ()
    {
        start        = 0;
        viewportSize = viewport.rect.size;
        horizontal   = false;
        vertical     = false;
        switch (scrollType)
        {
            case ScrollType.Horizontal:
                spacing         = content.GetComponent<HorizontalLayoutGroup> ().spacing;
                constraintCount = 1;
                break;
            case ScrollType.Vertical:
                spacing         = content.GetComponent<VerticalLayoutGroup> ().spacing;
                constraintCount = 1;
                break;
            case ScrollType.VerticalGrid:
                var gridV = content.GetComponent<GridLayoutGroup> ();
                spacing         = gridV.spacing.y;
                constraintCount = gridV.constraintCount;
                break;
            case ScrollType.HorizontalGrid:
                var gridH = content.GetComponent<GridLayoutGroup> ();
                spacing         = gridH.spacing.x;
                constraintCount = gridH.constraintCount;
                break;
        }
        if (Application.isPlaying)
            StartCoroutine (NewItem (0));
    }

    protected override void OnDisable ()
    {
        for (var i = content.childCount - 1; i >= 0; i--)
            DestroyImmediate (content.GetChild (i).gameObject);
    }

    public override void OnDrag (PointerEventData eventData)
    {
        if (dragCount != 0)
        {
            var first = (RectTransform)content.GetChild (0);
            switch (scrollType)
            {
                case ScrollType.Vertical:
                    eventData.position -= new Vector2 (0, dragCount * (first.sizeDelta.y + spacing));
                    break;
                case ScrollType.Horizontal:
                    eventData.position += new Vector2 (dragCount * (first.sizeDelta.x + spacing), 0);
                    break;
                case ScrollType.VerticalGrid:
                    eventData.position -= new Vector2 (0, dragCount * (first.sizeDelta.y + spacing));
                    break;
                case ScrollType.HorizontalGrid:
                    eventData.position += new Vector2 (dragCount * (first.sizeDelta.x + spacing), 0);
                    break;
            }
        }
        base.OnDrag (eventData);
    }

    public override void OnEndDrag (PointerEventData eventData)
    {
        base.OnEndDrag (eventData);
        dragCount = 0;
    }
#endregion


    private void Update ()
    {
        if (content.childCount == 0)
            return;

        contentPosition = content.localPosition;
        CheckFirst ();
        CheckLast ();
        CheckOver ();
    }


#region Private
    private RectTransform NewNode (int i)
    {
        if (!loop && i >= dataCount)
            return null;

        var node = Instantiate (item, content);
        node.gameObject.SetActive (true);
        SetValue (node, i);
        end = i;
        return node;
    }

    private IEnumerator NewItem (int i)
    {
        yield return new WaitForEndOfFrame ();
        var node = NewNode (i);

        if (scrollType == ScrollType.VerticalGrid || scrollType == ScrollType.HorizontalGrid)
            for (var j = i + 1; j < i + constraintCount; j++)
                node = NewNode (j);

        if (node == null)
            yield break;

        yield return new WaitForEndOfFrame ();
        switch (scrollType)
        {
            case ScrollType.Vertical:
            {
                if (-node.localPosition.y < viewportSize.y)
                    StartCoroutine (NewItem (i + 1));
                else
                    vertical = true;
                break;
            }
            case ScrollType.Horizontal:
            {
                if (node.localPosition.x < viewportSize.x)
                    StartCoroutine (NewItem (i + 1));
                else
                    horizontal = true;
                break;
            }
            case ScrollType.VerticalGrid:
            {
                if (-node.localPosition.y < viewportSize.y)
                    StartCoroutine (NewItem (i + constraintCount));
                else
                    vertical = true;
                break;
            }
            case ScrollType.HorizontalGrid:
            {
                if (node.localPosition.x < viewportSize.x)
                    StartCoroutine (NewItem (i + constraintCount));
                else
                    horizontal = true;
                break;
            }
        }
    }

    private void SetValue (Transform t, int i)
    {
        i      = GetIndex (i);
        t.name = i.ToString ();
        callback?.Invoke (t, i);
    }

    private int GetIndex (int i)
    {
        if (i < 0)
        {
            while (i < 0)
                i += dataCount;
            return i;
        }

        if (i < dataCount)
            return i;

        while (i >= dataCount)
            i -= dataCount;
        return i;
    }

    private void CheckFirst ()
    {
        var first = (RectTransform)content.GetChild (0);
        var pos   = first.localPosition;
        var size  = first.sizeDelta;
        var fx = scrollType switch
        {
            ScrollType.Vertical       => -pos.y - contentPosition.y + size.y,
            ScrollType.Horizontal     => pos.x + contentPosition.x + size.x,
            ScrollType.VerticalGrid   => -pos.y - contentPosition.y + size.y,
            ScrollType.HorizontalGrid => pos.x + contentPosition.x + size.x,
            _                         => 0
        };

        if (fx > 0)
            return;

        if (!loop && end >= dataCount - 1)
            return;

        dragCount++;
        for (var i = 0; i < constraintCount; i++)
        {
            start++;
            end++;
            first = (RectTransform)content.GetChild (0);
            SetValue (first, end);
            first.SetAsLastSibling ();
            first.gameObject.SetActive (loop || end < dataCount);
        }
    }

    private void CheckLast ()
    {
        var index = content.childCount - constraintCount - 1;
        if (index < 0)
            return;

        var last = (RectTransform)content.GetChild (index);
        var pos  = last.localPosition;
        var size = last.rect.size;
        var lx = scrollType switch
        {
            ScrollType.Vertical       => -pos.y - contentPosition.y - viewportSize.y + GetRemainder (viewportSize.y, size.y + spacing) - spacing,
            ScrollType.Horizontal     => pos.x + contentPosition.x - viewportSize.x + GetRemainder (viewportSize.x, size.x + spacing) - spacing,
            ScrollType.VerticalGrid   => -pos.y - contentPosition.y - viewportSize.y + GetRemainder (viewportSize.y, size.y + spacing) - spacing,
            ScrollType.HorizontalGrid => pos.x + contentPosition.x - viewportSize.x + GetRemainder (viewportSize.x, size.x + spacing) - spacing,
            _                         => 0
        };

        if (lx <= 0)
            return;

        if (!loop && start <= 0)
            return;

        dragCount--;
        for (var i = 0; i < constraintCount; i++)
        {
            start--;
            end--;
            last = (RectTransform)content.GetChild (content.childCount - 1);
            SetValue (last, start);
            last.SetAsFirstSibling ();
            last.gameObject.SetActive (loop || start >= 0);
        }
    }

    private void CheckOver ()
    {
        switch (scrollType)
        {
            case ScrollType.Vertical:
                if (contentPosition.y < -spacing)
                    content.localPosition = new Vector2 (0, -spacing);
                if (contentPosition.y > content.rect.height - viewportSize.y)
                    content.localPosition = new Vector2 (0, content.rect.height - viewportSize.y);
                break;
            case ScrollType.Horizontal:
                if (contentPosition.x > spacing)
                    content.localPosition = new Vector2 (spacing, 0);
                if (contentPosition.x < viewportSize.x - content.rect.width)
                    content.localPosition = new Vector2 (viewportSize.x - content.rect.width, 0);
                break;
            case ScrollType.VerticalGrid:
                if (contentPosition.y < -spacing)
                    content.localPosition = new Vector2 (0, -spacing);
                if (contentPosition.y > content.rect.height - viewportSize.y)
                    content.localPosition = new Vector2 (0, content.rect.height - viewportSize.y);
                break;
            case ScrollType.HorizontalGrid:
                if (contentPosition.x > spacing)
                    content.localPosition = new Vector2 (spacing, 0);
                if (contentPosition.x < viewportSize.x - content.rect.width)
                    content.localPosition = new Vector2 (viewportSize.x - content.rect.width, 0);
                break;
        }
    }

    //取余（整除时返回除数，而不是0）
    private static float GetRemainder (float a, float b)
    {
        while (a > b)
            a -= b;
        return a;
    }
#endregion
}