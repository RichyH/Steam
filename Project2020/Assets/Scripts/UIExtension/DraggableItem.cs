﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DraggableItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public bool reset;
    public string dropName;
    public GameObject dropTarget;
    public Action<GameObject, GameObject> callback = (drag, drop) => { Debug.Log(drag.name + " drop on " + drop.name); };

    private          Vector2             start, from, to;
    private readonly List<RaycastResult> list = new List<RaycastResult>();
    
    public void OnBeginDrag(PointerEventData eventData)
    {
        from  = eventData.position;
        start = transform.position;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!GetComponent<Image>().raycastTarget) return;
        to = eventData.position;
        transform.position = start + to - from;
        list.Clear();
        dropTarget = null;
        GetComponentInParent<GraphicRaycaster>().Raycast(eventData, list);
        list.ForEach ((rr) =>
        {
            if (string.IsNullOrEmpty (dropName) || rr.gameObject.name == dropName)
                dropTarget = rr.gameObject;
        });
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (reset || dropTarget == null)
            transform.position = start;
        if (dropTarget)
        {
            callback(gameObject, dropTarget);
            dropTarget = null;
        }
    }

    public void Reset()
    {
        transform.position = start;
    }
}
