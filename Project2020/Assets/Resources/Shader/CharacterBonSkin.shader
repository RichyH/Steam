Shader "CharacterBoneSkin"
{
    Properties
    {
        //主帖图
        _MainTex("MainTex", 2D) = "white" {}
        //动画贴图
        _AnimMap("AnimMap", 2D) = "white" {}
        //x=骨骼数，y=帧率，z=特效id，w=缩放
        _AniBase("AniBase",Vector) = (0,0,0,0)
        //x=动作id，y=起始帧，z=总帧数，w=当前帧
        _AniData("AniData",Vector) = (0,0,0,0)
    }

    SubShader{
        Pass{
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #pragma target 4.5
            #pragma multi_compile _ DOTS_INSTANCING_ON

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 normal : NORMAL;
                float2 uv : TEXCOORD0;
                float4 uv2 : TEXCOORD1;
                uint vid : SV_VertexID;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct VertexAnimated{
                float3 vertex;
                float3 normal;   
            };

CBUFFER_START(UnityPerMaterial)
            float4 _MainTex_ST;
            float4 _AnimMap_TexelSize;
            float4 _AniBase;
            float4 _AniData;
CBUFFER_END
            TEXTURE2D(_MainTex);SAMPLER(sampler_MainTex);
            sampler2D _AnimMap;

#ifdef UNITY_DOTS_INSTANCING_ENDABLED
    UNITY_DOTS_INSTANCING_START(MaterialPropertyMetadata)
    UNITY_DOTS_INSTANCED_PROP(float4,_AniBase);
    UNITY_DOTS_INSTANCED_PROP(float4,_AniData);
    UNITY_DOTS_INSTANCING_END(MaterialPropertyMetadata)
#endif
            float4 IndexToUV(float i,float4 size){
                float row = i / size.z;
                float col = i % size.z;
                return float4(col/size.z,fmod(row/size.w,1),0,0);
            }
            float4x4 GetMatFromPosAndRot(float4 pos,float4 r){
                float l = 2 / length(r);
                return float4x4(
                    l*r.x * r.x + l*r.w * r.w - 1, l*r.x * r.y - l*r.z * r.w,    l*r.x * r.z + l*r.y * r.w,     pos.x,
                    l*r.x * r.y + l*r.z * r.w,     l*r.y * r.y + l*r.w * r.w - 1,l*r.y * r.z - l*r.x * r.w,     pos.y,
                    l*r.x * r.z - l*r.y * r.w,     l*r.y * r.z + l*r.x * r.w,    l*r.z * r.z + l*r.w * r.w - 1, pos.z,
                    0,0,0,1);
            }
            float4x4 GetMat(sampler2D animMap,float4 mapSize,float tarBoneIdx){
                float row0Idx = 2 * tarBoneIdx;
                float row1Idx = row0Idx + 1;
                float4 pos = tex2Dlod(animMap,IndexToUV(row0Idx,mapSize));
                float4 dot = tex2Dlod(animMap,IndexToUV(row1Idx,mapSize));
                float4 rot = float4(dot.x*2-1,dot.y*2-1,dot.z*2-1,dot.w*2-1);
                return GetMatFromPosAndRot(pos,rot);
            }
            //骨骼动画计算顶点位置
            VertexAnimated AnimateVertex(
                sampler2D animMap,float4 animMapTexelSize,
                float4 uv,float4 position,float4 normalOS,
                float4 aniData,int BoneCount)
            {
                float index = round(_Time.y*100)%aniData.z;
                float frameIndex = aniData.y+index;
                float4x4 mat =  GetMat(animMap,animMapTexelSize,round((frameIndex+uv.y)*BoneCount));
                float4x4 mat2 = GetMat(animMap,animMapTexelSize,round((frameIndex+uv.w)*BoneCount));
                VertexAnimated output;
                output.vertex = mul(mat,position) * uv.x + mul(mat2,position) * uv.z;
                output.normal = mul(mat,normalOS) * uv.x + mul(mat2,normalOS) * uv.z;
                return output;
            }
            
            v2f vert (appdata v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);

                #ifdef UNITY_DOTS_INSTANCING_ENDABLED
                    float4 aniBase = UNITY_ACCESS_DOTS_INSTANCED_PROP(float4,_AniBase);
                    float4 aniData = UNITY_ACCESS_DOTS_INSTANCED_PROP(float4,_AniData);
                #else
                    float4 aniBase = _AniBase;
                    float4 aniData = _AniData;
                #endif
                aniData.w = _Time.y;
                VertexAnimated vertex_animated = AnimateVertex(
                    _AnimMap,_AnimMap_TexelSize,v.uv2,v.vertex,v.normal,aniData,aniBase.x);
                o.vertex = GetVertexPositionInputs(vertex_animated.vertex).positionCS;
                o.uv = v.uv;
                return o;
            }
            
            half4 frag (v2f i) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(i);
                half4 col = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex,TRANSFORM_TEX(i.uv,_MainTex));
                return col;
            }
            ENDHLSL
        }
    }
}
