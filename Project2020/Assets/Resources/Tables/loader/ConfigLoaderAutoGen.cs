using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Collections.Concurrent;
using Newtonsoft.Json;

namespace Pluto.Config
{
    public partial class ConfigLoader
    {
        public ConfigLoader(string path)
        {
            Init(path);
        }
        public void Init(string path)
        {
            path += "\\";
            if (File.Exists(path + "ConfigMaterial.json"))
            {
                var data = File.ReadAllText(path + "ConfigMaterial.json", Encoding.UTF8);
                _id2ConfigMaterial = JsonConvert.DeserializeObject<ConcurrentDictionary<int, ConfigMaterial>>(data);
                _filename2Md5.TryAdd("ConfigMaterial", PlutoConfig.Util.MD5.Encrypt(path + "ConfigMaterial.json"));
            }
        }

        private ConcurrentDictionary<int, ConfigMaterial> _id2ConfigMaterial = new ConcurrentDictionary<int, ConfigMaterial>();

        private ConcurrentDictionary<string, string> _filename2Md5 = new ConcurrentDictionary<string, string>();
    }
}
