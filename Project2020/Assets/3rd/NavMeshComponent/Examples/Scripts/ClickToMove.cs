using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

// Use physics raycast hit from mouse click to set agent destination
[RequireComponent(typeof(NavMeshAgent))]
public class ClickToMove : MonoBehaviour
{
    Vector3 delta;
    Mine mine;
    Camera Camera;
    Transform Building;
    public MinePop minePop;
    Bullet bullet;
    public static int BuildingID = -1;
    NavMeshAgent m_Agent;
    RaycastHit m_HitInfo = new RaycastHit();

    void Start()
    {
        Camera = Camera.main;
        bullet = Resources.Load<Bullet>("Prefabs/Bullet");
        Building = Resources.Load<Transform>("Prefabs/Grid");
        delta = Camera.transform.position - transform.position;
        m_Agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out m_HitInfo))
            {
                Vector2 grid = SquareGrid.Real2Grid(m_HitInfo.point.x, m_HitInfo.point.z);
                Debug.Log("移动到坐标" + grid);
                Vector2 pos = SquareGrid.Grid2Real(grid);
                m_Agent.destination = new Vector3(pos.x, 0.1f, pos.y);
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (BuildingID < 0 || EventSystem.current.IsPointerOverGameObject()) return;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo))
            {
                GameObject gameObj = hitInfo.collider.gameObject;
                //int x = Mathf.FloorToInt(hitInfo.point.x / Const.grid);
                //int z = Mathf.FloorToInt(hitInfo.point.z / Const.grid);
                //Vector2 grid = HexagonGrid.Real2Grid(hitInfo.point.x, hitInfo.point.z);
                //Vector2 pos = HexagonGrid.Grid2Real(grid);
                Debug.Log("放置建筑" + BuildingID + "到" + hitInfo.point);
                Transform t = Instantiate(Building);
                t.name = "建筑" + BuildingID;
                t.gameObject.SetActive(true);
                t.GetComponent<SquareGrid>().x =(int)hitInfo.point.x;
                t.GetComponent<SquareGrid>().y = (int)hitInfo.point.z;
                Tools.SetImage3D(t.gameObject, BuildingItem.path[BuildingID]);
                BuildingID = -1;
            }
        }
        if (Input.GetKeyUp(KeyCode.F))
        {
            Transform t = Instantiate(bullet).transform;
            t.position = transform.position;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo))
            {
                t.forward = Vector3.Normalize(hitInfo.point - transform.position);
            }
            t.gameObject.SetActive(true);
        }
        //镜头控制
        Camera.transform.position = new Vector3(
            transform.position.x + delta.x,
            Camera.transform.position.y - Input.GetAxis("Mouse ScrollWheel"),
            transform.position.z + delta.z);
    }


    private void OnTriggerEnter(Collider other)
    {
        mine = other.GetComponent<Mine>();
        if (mine != null)
        {
            minePop.Open(mine);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Mine>() != null)
        {
            minePop.gameObject.SetActive(false);
        }
    }
}
