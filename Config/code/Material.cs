using System;
using System.Collections.Generic;

namespace Pluto.Config
{
    /// <summary>
    /// 材料
    /// </summary>
    [Serializable]
    public partial class ConfigMaterial
    {
        /// <summary>
        /// ID
        /// </summary>
        public int ID;

        /// <summary>
        /// 材料名
        /// </summary>
        public string Name;

        /// <summary>
        /// 硬度
        /// </summary>
        public int Hardness;

        /// <summary>
        /// 状态
        /// </summary>
        public ConfigMaterialState State;
    }

    /// <summary>
    /// 材料状态
    /// </summary>
    public enum ConfigMaterialState
    {
        /// <summary>
        /// 无效状态
        /// </summary>
        None = 0,

        /// <summary>
        /// 固态
        /// </summary>
        Solid = 1,

        /// <summary>
        /// 液态
        /// </summary>
        Liquid = 2,

        /// <summary>
        /// 气态
        /// </summary>
        Gas = 3,
    }

    /// <summary>
    /// 材料常量
    /// </summary>
    public static class ConfigMaterilConst
    {
    }
}
